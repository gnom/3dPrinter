$fn=100;

include <arc.scad>

difference() {
    union() {
        //Ausenwand Zylinder
        cylinder(d=57, h=55, center=true);

        // Rastnasen Anfang
        for(i=[0:3]) {
            rotate([0, 0, 90*i])
                translate([26, 0, -20]) {
                    rotate([0,90,0]) {
                        cylinder(d=5, h=12, center=true);
                    }
                }
        }
        // Rastnase Ende
    }
    union() {

        // Innenwand Zylinder
        cylinder(d=52, h=70, center=true);

        // Aussparung Anfang

        // Aussparungen Schlitze
        // endanschläge
        for(i=[0:3]) {

            rotate([0,0,i*90]) {
                translate([27.5,0,26.5]) {
                    cube([7,6,12.5], center=true);
                }
                translate([27,0,20]) {
                    rotate([0,90,0]) {
                        cylinder(d=6, h=10, center=true);
                    }
                }
            }
            rotate([0,0,90*i+25]) {
                translate([0,55/2,20]) {
                    rotate([90,0,0]) {
                        cylinder(d=6, h=10, center=true);
                    }
                }
                translate([0,0,17]) {
                    arc(6, 6, 30, 25);
                }
            }
        }
        // Aussparungen Ende
    }
}
