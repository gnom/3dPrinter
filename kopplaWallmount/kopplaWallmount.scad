$fn=200;

difference() {
    cube([60,20,44], center=true);
    translate([0,3,3]) cube([54,17,44], center=true);
    translate([0,25,-20]) cylinder(d=45, h=10, center=true);    
}
difference() {
    translate([39,0,19.5]) cube([18,20,5], center=true);
    translate([39,0,20]) cylinder(d=5, h=10, center=true);
}

difference() {
    translate([-39,0,19.5]) cube([18,20,5], center=true);
    translate([-39,0,20]) cylinder(d=5, h=10, center=true);
}
