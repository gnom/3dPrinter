$fn=100;

union() {
    minkowski() {
        cube([18,18,1], center=true);
        cylinder(r=5, h=1, center=true);
    }
    for(i=[0:3]) {
        rotate([0,0,i*90]) {
            translate([0,-14, 1]) {
                linear_extrude(height=5, scale=0.9) {
                    polygon([[-2.5,0],[2.5,0],[2.5,5],[4,5],[4,7],[2.5,9],[-2.5,9],[-4,7],[-4,5],[-2.5,5]]);

                }
            }
        }
    }
    translate([0,0,1]) {
        cylinder(d1=5.2, d2=4, h=7);
    }
}
