$fn=200;

use <roundedCube.scad>
use <wedge.scad>

// TODO Aussparung für USB-Anschluss

//difference() {
//    // Gehäusewürfel
//    union() {
//        roundedCube([106,65,71], r=2, center=true);
//        //Wandbefestigung
//        difference() {
//            rotate([90,0,0]) {
//                hull() {
//                    translate([-63,0,-30.5]) cylinder(r=8, h=4, center=true);
//                    translate([63,0,-30.5]) cylinder(r=8, h=4, center=true);
//                }
//            }
//        }
//    }
//    //Schraublöcher
//    translate([63,30.5,0]) rotate([90,0,0]) cylinder(r2=4, r1=2, h=6, center=true);
//    translate([-63,30.5,0]) rotate([90,0,0]) cylinder(r2=4, r1=2, h=6, center=true);
//    //Hohlraum
//    translate([0, 0, 1]) cube([100,59,67], center=true);
//    // Aussparung für Deckel
//    translate([0,0,35]) cube([102,61,3], center=true);
//    translate([22, 18.5,-31]) cube([10,8,10], center=true);
//}
//
//difference() {
//    // Abstützung Deckel
//    union() {
//        translate([0,28.5,29.5]) rotate([0,180,90]) wedge(4,6,100);
//        translate([0,-28.5,29.5]) rotate([0,180,-90]) wedge(4,6,100);
//        difference() {
//            //Aussparung für PCB
//            translate([49,0,29.5]) rotate([0,180,0]) wedge(4,6,59);
//            translate([48.5,17.5,29.5]) cube([5,2,10], center=true);
//        }
//        difference() {
//            //Aussparung für PCB
//            translate([-49,0,29.5]) rotate([0,180,180]) wedge(4,6,59);
//            translate([-48.5,17.5,29.5]) cube([5,2,10], center=true);
//        }
//        // Halterschienen für PCB
//        translate([48.5, 20, -1]) cube([3,3,67], center=true);
//        translate([48.5, 15, -1]) cube([3,3,67], center=true);
//        translate([-48.5, 20, -1]) cube([3,3,67], center=true);
//        translate([-48.5, 15, -1]) cube([3,3,67], center=true);
//    }
//    //Aussparungen PCB Einschub
//    translate([0,15,29]) rotate([180,90,90]) wedge(7.1,3.1,200);
//    translate([34, 27,31]) cube([8,8,8], center=true);
//    translate([0, 27,31]) cube([20,8,8], center=true);
//}
//
//translate([0,-31.5,0]) {
//    rotate([90,0,0]) { 
//        translate([0,7,0]) linear_extrude(2) text(text="HINTERTÜR", valign="center", halign="center", size=10, font="DIN 1451 Mittelschrift:style=Regular");
//        translate([0,-7,0]) linear_extrude(2) text(text="status.ccc-ffm.de", valign="center", halign="center", size=9, font="DIN 1451 Mittelschrift:style=Regular");
//    }
//}


        

// Deckel
difference() {
    union() {
        translate([0,0,33.5]) {
            cube([100,59,2], center=true);
            translate([0,0,1]) cube([102,61,2], center=true);
            // Beschriftungen
            translate([-30,5,0]) linear_extrude(3) text("open", valign="center", halign="center", font="DIN 1451 Engschrift:style=Regular");
            translate([0,7.5,0]) linear_extrude(3) text("member", valign="center", halign="center", font="DIN 1451 Engschrift:style=Regular");
            translate([30,7.5,0]) linear_extrude(3) text("close", valign="center", halign="center", font="DIN 1451 Engschrift:style=Regular");
        }
    }
    // Löcher für die Taster
    translate([0,-14, 34]) cylinder(d=16, h=5, center=true);
    translate([-30,-14, 34]) cylinder(d=16, h=5, center=true);
    translate([30,-14, 34]) cylinder(d=16, h=5, center=true);
}


