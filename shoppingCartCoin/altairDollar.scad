$fn=200;

include <Write.scad>

linear_extrude(2.3, center=true)
text("a", valign="center", halign="center", size=15.5, font="DejaVu Sans:style=Bold", spacing=1);
cube([1.5,16,2.3], center=true);

difference() {
    cylinder(d=23.25, h=2.3, center=true);
    translate([0,0,1])
    cylinder(d=21, h=1, center=true);
}

