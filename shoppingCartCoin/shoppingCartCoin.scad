$fn=200;

linear_extrude(height=2.3, center=true)
text("dd2", valign="center", halign="center", size=7.5, font="DIN 1451 Engschrift", spacing=1.1);

difference() {
    cylinder(d=23.25, h=2.3, center=true);
    translate([0,0,1])
    cylinder(d=21, h=1, center=true);
}