$fn=150;

difference() {
    union() {
        for (i=[0:5]) {
            rotate([0,0,(360/6)*i]) {
                hull() {
                    translate([-30,0,0]) {
                        intersection() {
                            cylinder(d=40, h=10, center=true);
                            sphere(d=40, center=true);
                        }
                    }
                    translate([30,0,0]) {
                        intersection() {
                            cylinder(d=40, h=10, center=true);
                            sphere(d=40, center=true);
                        }
                    }
                }
            }
        }
        translate([0,0,-25]) {
            cylinder(d1=15, d2=76.5, h=20);
        }
    }
    for (i=[0:5]) {
        rotate([0,0,(360/6)*i]) {
            translate([-30,0,0]) {
                cylinder(d=25, h=60, center=true);
            }
        }
    }
    cylinder(d=6.5, h=60, center=true);
    translate([0,0,-26]) {
        cylinder(d=11, h=4, $fn=6);
    }
//    translate([0,40,-35]) {
//        rotate([0,90,0]) {
//            #circle(d=, center=true);
//        }
//    }
}