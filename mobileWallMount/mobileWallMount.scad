$fn=40;

use <roundedCube.scad>;

difference() {
    roundedCube([150,70,30], r=3);
    difference() {
        translate([0,-10,5]) rotate([-10,0,0]) cube([152,30,40], center=true);
        translate([0,-10,-25]) cube([154,60,30], center=true);
    }
    translate([0,10,5]) cube([152,30,30], center=true);
    translate([-73,12,-15]) cube([25,4,20], center=true);
    translate([-60,12,-14]) cylinder(d2=10, d1=7, h=9, center=true);
    translate([-73,-12,-15]) cube([25,4,20], center=true);
    translate([-60,-12,-14]) cylinder(d2=10, d1=7, h=9, center=true);
}
translate([40,0,0]) {
    difference() {
        translate([0,30,23]) rotate([90,0,0]) roundedCube([25,30,10], flat=true);
        translate([0,30,25.5]) rotate([90,0,0]) cylinder(d=4.5, h=20, center=true);
        translate([0,26.499,25.5]) rotate([90,0,0]) cylinder(d2=8.2, d1=4.5, h=3, center=true);
    }
}
translate([-40,0,0]) {
    difference() {
        translate([0,30,23]) rotate([90,0,0]) roundedCube([25,30,10], flat=true);
        translate([0,30,25.5]) rotate([90,0,0]) cylinder(d=4.5, h=20, center=true);
        translate([0,26.499,25.5]) rotate([90,0,0]) cylinder(d2=8.2, d1=4.5, h=3, center=true);
    }
}
