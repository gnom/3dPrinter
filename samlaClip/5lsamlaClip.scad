$fn=200;

use <roundedCube.scad>

l=30;
w=17;
h=23;


rotate([90,90,0]) {
union() {
difference() {
    cube([l,w,h], center=true); 
    translate([0,0.5,2.5])
    difference() {
       rotate([0,90,0]) cylinder(r=7, h=l+2, center=true);
       translate([0,4,0]) cube([l+4, 8, 15], center=true);
    }
    translate([0,-5.5,-8.5]) rotate([0,90,0]) cylinder(h=l+2, r=1, center=true);
    translate([0,2,-3.5]) cube([l+2, w, 10.5], center=true);
    translate([0,2,-7.5]) cube([l+2, 15, 4], center=true);
    translate([0,5,-10]) cube([l+2, 9, 4], center=true);
    translate([0,3.5,6.5]) cube([l+2,6.01, 6], center=true);
    translate([0,-3.50,0]) cube([l+2,6, 5], center=true);
    translate([0, -4.5, 7.5]) difference() {
        cube([l+2, 10, 10], center=true);
        translate([0,5,-5]) rotate([0,90,0]) cylinder(r=9, h=l+4, center=true);
    }
    translate([0, 8.5, 1.75]) rotate([180,0,0]) difference() {
        cube([l+2, 4, 4], center=true);
        translate([0,2,-2]) rotate([0,90,0]) cylinder(r=2, h=l+4, center=true);
    }
    translate([0, 8.5, 11.5]) rotate([-90,0,0]) difference() {
        cube([l+2, 4, 4], center=true);
        translate([0,2,-2]) rotate([0,90,0]) cylinder(r=2, h=l+4, center=true);
    }
    translate([0, -8.5, -11.5]) rotate([90,0,0]) difference() {
        cube([l+2, 4, 4], center=true);
        translate([0,2,-2]) rotate([0,90,0]) cylinder(r=2, h=l+4, center=true);
    }
    translate([0,0.5,2]) roundedCube([l-15,8,4],3,true);
    translate([0,-0.5,-10]) roundedCube([l-15,8,4],3,true);
}
translate([0,-7.5,-1.5]) rotate([90,180,0])linear_extrude(2) text("GNOM", valign="center", halign="center", size=9, font="DIN Schablonierschrift:style=Regular", spacing=1);
}
}