$fn=200;

use <roundedCube.scad>

l=25;
w=22;
h=23;

rotate([90,90,0]) 
difference() {
    cube([l,w,h], center=true);  
    translate([0,-2,2.5])
    difference() {
       rotate([0,90,0]) cylinder(r=7, h=l+2, center=true);
       translate([0,4,0]) cube([l+4, 8, 15], center=true);
    }
    translate([0,-8,-8.5]) rotate([0,90,0]) cylinder(h=l+2, r=1, center=true);
    translate([0,2,-3.5]) cube([l+2, w, 10.5], center=true);
    translate([0,3,-7.5]) cube([l+2, w, 4], center=true);
    translate([0,6,-10]) cube([l+2, 19, 4], center=true);
    translate([0,6,6.5]) cube([l+2,6.01, 6], center=true);
    translate([0,-6,0]) cube([l+2,6, 5], center=true);
    translate([0,0.5,4.5]) cube([l+2,5.1,10], center=true);
    translate([0, -7, 7.5]) difference() {
        cube([l+2, 10, 10], center=true);
        translate([0,5,-5]) rotate([0,90,0]) cylinder(r=9, h=l+4, center=true);
    }
    translate([0, 11, 1.75]) rotate([180,0,0]) difference() {
        cube([l+2, 4, 4], center=true);
        translate([0,2,-2]) rotate([0,90,0]) cylinder(r=2, h=l+4, center=true);
    }
    translate([0, 11, 11.5]) rotate([-90,0,0]) difference() {
        cube([l+2, 4, 4], center=true);
        translate([0,2,-2]) rotate([0,90,0]) cylinder(r=2, h=l+4, center=true);
    }
    translate([0, -11, -11.5]) rotate([90,0,0]) difference() {
        cube([l+2, 4, 4], center=true);
        translate([0,2,-2]) rotate([0,90,0]) cylinder(r=2, h=l+4, center=true);
    }
    if(l>=25) {
        translate([0,3,2]) roundedCube([l-15,8,4],3,true);
        translate([0,-3,-10]) roundedCube([l-15,8,4],3,true);
    }
}
