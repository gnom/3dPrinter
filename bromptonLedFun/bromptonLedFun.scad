$fn=300;

//Brompton LED 

height = 15;

difference() {
    cube([51,51,height], center=true);
    cylinder(d=44.8, h=height+1, center=true);
    difference() {
        cube([52,52,height+1], center=true);
        cylinder(d=51,h=height+2, center=true);
    }
    translate([22,0,0]) {
        cube([20,36,height+1], center=true);
    }
    translate() {
           rotate() {
               cube([10,10,height+1], center=true);
           }
       }
}

rotate([0,0,18]) {
    difference() {
        translate([-28,0,0]) {
            cube([9,18,height], center=true);
        }
        translate([-28,0,0]) {
            cube([4.8,12.7,height+1], center=true);
        }
    }
}

rotate([0,0,-18]) {
    difference() {
        translate([-28,0,0]) {
            cube([9,18,height], center=true);
        }
        translate([-28,0,0]) {
            cube([4.8,12.7,height+1], center=true);
        }
    }
}

translate([-30.688,0,0]) {
    cube([6,2.98,height], center=true);
}