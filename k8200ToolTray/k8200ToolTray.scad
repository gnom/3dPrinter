$fn=200;

difference() {
    union() {
        minkowski() {
            translate([25,90,0]) cylinder(r=5, h=13.5, center=true);
            cube([50, 180, 13.5], center=true);
        }
        translate([50,10,0.5]) cube([21, 160, 13]);
        //translate([57.5, 90, -26]) cube([15, 160, 2], center=true);
        translate([-3.5,90,0]) {
            rotate([90,0,90]) {
                mirror([180,0,0]) {
                    linear_extrude(height=5, center=true) {
                        text("SLARTIBARTFAST", size=15, halign="center", valign="center", font="DIN 1451 Mittelschrift:style=Regular");
                     }
                 }
             }
         }
     }
    translate([60,0,-11.5]) {
        hull() {
            cube([6,180,20]);
            translate([0,180,15]) rotate([90,0,0]) cylinder(r=5, 180);
        }
    }
    translate([57,90,2]) cube([4,180,6], center=true);
    translate([0,0,4.5])
    minkowski() {
        translate([25,90,0]) cylinder(r=5, h=13.5, center=true);
        cube([44, 174, 13.5], center=true);
    }
    for(i=[1:3]) {
        translate([65,50*i-10,0]) {
            cube([20,40,40], center=true);
        }
    }
}