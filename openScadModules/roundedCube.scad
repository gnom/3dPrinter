module roundedCube(dim, r=1, flat=false) {
    translate([-dim[0]/2+r,-dim[1]/2+r,-dim[2]/2]) {
        if (flat) {
            minkowski() {
                cube([dim[0]-(r*2),dim[1]-(r*2),dim[2]/2]);
                cylinder(r=r, h=dim[2]/2);
            }
        } else {
            minkowski() {
                cube([dim[0]-(r*2),dim[1]-(r*2),dim[2]-(r*2)]);
                sphere(r=r);
            }
        }
    }
}

