module wedge(w, h, l) {
    translate([-w/2,l/2,-h/2])
    rotate([90,0,0])
    linear_extrude(l) polygon([[0,0], [w,0], [0,h]]);
}