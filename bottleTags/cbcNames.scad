$fn=300;

include <Write.scad>

difference() {
    union() {
        translate([0,0,2.5]) {
            difference() {
                cylinder(d2=57, d1=61.5, h=5, center=true);
                cylinder(d2=53, d1=57.5, h=5.01, center=true);
            }
        }
        translate([0,0,-11]) {
            difference() {
                union() {
                    cylinder(d=61.5, h=22, center=true);
                    rotate([0, 0,-90]) writecylinder("telegnom", [0,0,0], 25.5, 0, h=8, t=12, font="orbitron.dxf");
                }
                cylinder(d=57.5, h=22.01, center=true);
            }
        }
    }
    translate([30,0,-9]) {
        rotate([0,0,45]) {
            cube([57,57,30], center=true);
        }
    }
}