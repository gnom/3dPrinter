# MateClips

## Requirements

* openscad
* [Write.scad](http://www.thingiverse.com/thing:16193)
* sed

## run
* `./maketag.sh nickname`
* take make_nickname.stl to your printer
* never forget your mate again

## stolen from
[original work from daniel@thingiverse](http://www.thingiverse.com/thing:18978)
