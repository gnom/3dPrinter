#!/bin/bash

mkdir -p build

if [[ -z $1 ]]; then
	echo -e "\nUsage: ./maketag.sh nickname [2ndNickName] [nthNickName]\n"
	exit 1
fi

if [[ ! $(which openscad) ]]; then
	echo "Please install openscad!"
fi

for i in "$@"; do
	echo $i
	./mate.sh $i &
	./mio.sh $i &
	./water.sh $i &
	./fritz.sh $i &
	./bizzl.sh $1 &
done
