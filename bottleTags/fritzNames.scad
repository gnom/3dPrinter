$fn = 100;
include <Write.scad>

module name_tag () {
	difference() {
		union () {
			cylinder(r1=17, r2=15, h=26);
			rotate([0, 0, -45]) writecylinder(name, [0,0,0], 17, 26, h=8, t=3, font="orbitron.dxf");
		}
		cylinder(r1=15, r2=13, h=26.1);
		cylinder(r=15, h=1, center=true);
		difference () {
			cylinder(r1=24.5, r2=20, h=26);
			cylinder(r1=17.7, r2=15.7, h=26.1);
			cylinder(r=24, h=1, center=true);
		}
		translate([0,0,-1]) cube([50,50,50]);
	}
}

translate([-22,  22, 0]) name_tag(name="NickName");
