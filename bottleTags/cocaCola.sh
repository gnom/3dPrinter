#!/bin/bash

mkdir -p build

if [[ -z $1 ]]; then
	echo -e "\nUsage: ${0} nickname [2ndNickName] [nthNickName]\n"
	exit 1
fi

if [[ ! $(which openscad) ]]; then
	echo "Please install openscad!"
fi

kind=$(basename "$0")
kind="${kind%.*}"

for i in "$@"; do
	echo $i
	sed "s/NickName/$i/" ${kind}Names.scad > ${kind}_tmp.scad
	openscad -o build/${kind}_${i}.stl ${kind}_tmp.scad > /dev/null 2>&1
	rm ${kind}_tmp.scad
done
