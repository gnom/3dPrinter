$fn = 500;
include <Write.scad>

module name_tag () {
	difference() {
		union () {
			//outer cylinder
			cylinder(r1=29, r2=17, h=24);
			rotate([0, 0, -45]) writecylinder(name, [0,0,0], 18, 28, h=8, t=12, font="orbitron.dxf");
		}
		//inner cylinder
		cylinder(r1=27, r2=15, h=24.1);
		cylinder(r=50, h=1, center=true);
		difference () {
			cylinder(r1=40, r2=20, h=24);
			cylinder(r1=29.7, r2=17.7, h=24.1);
			//cylinder(r=80, h=1, center=true);
		}
		translate([0,0,-1]) cube([50,50,50]);
	}
}

translate([-22,  22, 0]) name_tag(name="NickName");
