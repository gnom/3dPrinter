
$fn = 100;
include <Write.scad>

module name_tag () {
	difference() {
		union () {
			
            cylinder(r1=24, r2=17.5, h=26);
			rotate([0, 0, -45]) writecylinder(name, [0,0,0], 18, 26, h=8, t=13, font="orbitron.dxf");
		}
		cylinder(r1=22, r2=15.5, h=26.1);
		cylinder(r=47.5, h=1, center=true);
		difference () {
			cylinder(r1=35.5, r2=30, h=26);
			cylinder(r1=24.2, r2=18.2, h=26.1);
			cylinder(r=25, h=1, center=true);
		}
		translate([0,0,-1]) cube([50,50,50]);
	}
}

translate([-22,  22, 0]) name_tag(name="NickName");
