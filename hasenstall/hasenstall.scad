$fn=200;
difference() {
    union() {
        cylinder(d=34, h=55, center=true);
        translate([0,10.5,10.5]) rotate([90,0,0]) cylinder(d=34, h=55, center=true);
    }
    translate([0,0,10.5]) difference() {
        translate([0,0,20]) cube([50,50,150],center=true);
        translate([0,0,-50]) cylinder(d=34, h=100, center=true);
        translate([0,50,0]) rotate([90,0,0]) cylinder(d=34, h=100, center=true);
        sphere(d=34, center=true);
    }
    translate([0,0,-15]) cylinder(d=28, h=27, center=true);
    translate([0,25.5,10.5]) rotate([90,0,0]) cylinder(d=28, h=27, center=true);
    translate([0,25,27]) cylinder(d=10, h=10, center=true);
    translate([0,-16,-14]) rotate([90,0,0]) cylinder(d=10, h=10, center=true);   
}
#translate([0,12.5,-2]) cube([18,4,4], center=true);