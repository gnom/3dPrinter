$fn=100;

difference() {
    union() {
        difference() {
            union() {
                cube([39,35,70], center=true);
                translate([-44,-6.23,-7.2]) rotate([-26.5,0,0]) translate([0,0,7]) cube([65,5,60], center=true);
            }
            translate([2.5,0,18]) cube([28,40,28], center=true);
            translate([4.5,0,-20]) cube([32,40,50], center=true);
            translate([-31.75,0,0]) {
                rotate([63.5,0,0]) translate([0,11.025,0]) hull(center=true) {
                    cylinder(d=4.5, h=20, center=true);
                    translate([0,4.5,0]) cylinder(d=4.5, h=20, center=true);
                }
            }
            translate([-63.75,0,0]) {
                rotate([63.5,0,0]) translate([0,11.025,0]) hull(center=true) {
                    cylinder(d=4.5, h=20, center=true);
                    translate([0,4.5,0]) cylinder(d=4.5, h=20, center=true);
                }
            }
            translate([-31.75,0,0]) {
                rotate([63.5,0,0]) translate([0,-21.025,0]) hull(center=true) {
                    cylinder(d=4.5, h=20, center=true);
                    translate([0,4.5,0]) cylinder(d=4.5, h=20, center=true);
                }
            }
            translate([-63.75,0,0]) {
                rotate([63.5,0,0]) translate([0,-21.025,0]) hull(center=true) {
                    cylinder(d=4.5, h=20, center=true);
                    translate([0,4.5,0]) cylinder(d=4.5, h=20, center=true);
                }
            }
            translate([-47.75,0,0]) rotate([63.5,0,0]) {
                translate([0,-3,0]) {
                    cylinder(d=35, h=20, center=true);
                }
            }
            rotate([-26.5,0,0]) translate([0,0,-37])cube([200,10,10], center=true);
        }

        translate([16,2.5,18]) cube([3,40,12],center=true);
        translate([-11,2.5,18]) cube([2,40,16],center=true);
        translate([-11,2.5,18]) cube([3,40,12],center=true);
        translate([16,2.5,18]) cube([2,40,16],center=true);

    }
    translate([0,20,-5]) rotate([-26.5,0,0]) cube([40,40,100], center=true);
}