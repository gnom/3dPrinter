$fn = 300;

difference() {
    union() {
        scale([1,1,0.5]) sphere(d=100);
        translate([0,0,-100])cylinder(d1=110, d2=100, h=100);
    }
    union() {
        scale([1,1,0.5]) sphere(d=98);
        translate([0,0,-100.1])cylinder(d1=108, d2=98, h=100.1);
    }
}