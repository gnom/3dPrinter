$fn=200;

difference() {
hull() {
    rotate([90,0,0]) minkowski() {
        sphere(d=5);
        cylinder(d=5, h=47, center=true);
    }
    translate([-34,0,0]) rotate([90,0,0]) minkowski() {
        cylinder(d=5, h=33, center=true);
        sphere(d=5);
    }
    translate([-90,0,0]) rotate([90,0,0]) minkowski() {
        sphere(d=5);
        cylinder(d=5, h=10, center=true);
    }
    
}
   translate([-5,0,0]) hull() {
       cube([1,34,20], center=true);
       translate([-24,0,0]) cube([1,25,20], center=true);
   }
   translate([-37,0,0]) cube([16.5,16.5,1.8], center=true);
}
translate([-60,0,5]) linear_extrude(0.3) rotate([0,0,180]) text(size=8, "ChaosPott", valign="center", halign="center", font="Oxygen\\-Sans:style=Sans\\-Book");