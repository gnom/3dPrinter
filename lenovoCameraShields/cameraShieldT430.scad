$fn=150;

difference() {
    linear_extrude(height=1) {
        polygon([[0,0],[0,22],[7,20.5],[7,1.5]]);
    }
    translate([3.5,15,0]) {
        cylinder(d=5, h=10, center=true);
    }   
}