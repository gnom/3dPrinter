$fn=150;

difference() {
    linear_extrude(height=0.6) {
        polygon([[0,-1.6],[0,24],[9,23],[9,-0.6]]);
    }
    translate([4,17,0]) {
        cylinder(d=5, h=10, center=true);
    }   
}