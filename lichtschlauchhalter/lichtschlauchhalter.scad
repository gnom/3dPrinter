$fn = 30;

height=12;

difference() {
    union() {
        cube([15.5,20.5,height], center=true);
        cylinder(d=15.5, h=height, center=true);
    }
    //magnets
    translate([-4,-9.2,2]) {
        rotate([90,0,0]) cylinder(d=5.4, h=2.2, center=true);
    }
    translate([4,-9.2,2]) {
        rotate([90,0,0]) cylinder(d=5.4, h=2.2, center=true);
    }
    translate([0,-9.2,-2]) {
        rotate([90,0,0]) cylinder(d=5.4, h=2.2, center=true);
    }
    cylinder(d=13.5, h=height+1, center=true);
    difference() {
        cube([20,20,height+1], center=true);
        cylinder(d=15.5, h=height+2, center=true);
        translate([0,-10.5,0]) {
            cube(21,11,height+2, center=true);
        }
    }
    translate([0,16,0]) {   
        cube([16,24,height+1], center=true);
    }
    translate([-6,4.2,0]) {
        rotate([0,0,-25]) {
            difference() {
                cube([2,2,height+1], center=true);
                translate([0,-1,0]) cylinder(d=1, h=height+2, center=true);
            }
        }
    }
    translate([6,4.2,0]) {
        rotate([0,0,25]) {
            difference() {
                cube([2,2,height+1], center=true);
                translate([0,-1,0]) cylinder(d=1, h=height+2, center=true);
            }
        }
    }
}

