$fn=100;
translate([0,0,1])
difference() {
	union() {
		difference() {
			union() {
				cube([95, 65, 5], center=true);
			}
			union() {
				translate([0, 0, 4]) {
					cube([90, 60, 10], center=true);
				}
				translate([45, 0, 4.75]) {
					cube([16, 13, 9], center=true);
				}
			}
		}
	}
}

translate([40,24.5,1.5]) {
	cylinder(h=3, d=3, center=true);
	cylinder(h=5, d=1, center=true);
}
translate([40,-24.5,1.5]) {
	cylinder(h=3, d=3, center=true);
	cylinder(h=5, d=1, center=true);
}

translate([-9,24.5,1.5]) {
	cylinder(h=3, d=3, center=true);
	cylinder(h=5, d=1, center=true);
}
translate([-9,-24.5,1.5]) {
	cylinder(h=3, d=3, center=true);
	cylinder(h=5, d=1, center=true);
}
translate([40, 0, 0.25]) {
	cube([11, 13, 2], center=true);
}
