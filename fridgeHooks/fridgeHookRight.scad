$fn=80;

module fridgeHook() {
    difference() {
        union() {
            difference() {
                cube([41,30,30], center=true);
                translate([0,-11.01,-2.6]) {
                    cube([15,8,25], center=true);
                }
            }
            translate([-19,-7.5,0]) {
                cube([10,45,30], center=true);
            }
        }
        translate([0,0,6.5]) {
            rotate([90,0,0]) {
                cylinder(d=3.8, h=50, center=true);
                translate([0,0,5.52]) {
                    cylinder(h=3, d2=7, d1=3.8, center=true);
                }
            }
        }
        translate([-19,0,7]) {
            rotate([90,0,0]) {
                cylinder(d=3.8, h=80, center=true);
                translate([0,0,28.51]) {
                    cylinder(h=3, d2=7, d1=3.8, center=true);
                }
            }
        }
        translate([14,0,7]) {
            rotate([90,0,0]) {
                cylinder(d=3.8, h=80, center=true);
                translate([0,0,13.51]) {
                    cylinder(h=3, d2=7, d1=3.8, center=true);
                }
            }
        }
        translate([0,7.01, 7.01]) {
            cube([50,16,16],center=true);
        }
        translate([-11,7,0]) {
            cylinder(d=3.8, h=50, center=true);
            translate([0,0,-13.501]) {
                cylinder(h=3, d1=7, d2=3.8, center=true);
            }
        }
        translate([9,7,0]) {
            cylinder(d=3.8, h=50, center=true);
            translate([0,0,-13.501]) {
                cylinder(h=3, d1=7, d2=3.8, center=true);
            }
        }
        translate([20.5,-15,0]) {
            difference() {
                cube([4,4,60], center=true);
                translate([-2,2,0]) {
                    cylinder(d=4, h=61, center=true);
                }
            } 
        }
    }
}

translate([30,0,0]) {
    fridgeHook();
}

translate([-30,0,0]) {
    mirror([1,0,0]) {
        fridgeHook();
    }
}