$fn=20;

module nupsies(n,d) {
    for(i=[0:360/n:360]) {
        echo(i);
        rotate(i, 0, 0) {
            translate([d,0,0]) {
                sphere(d=2);
            }
        }
    }
}

difference() {
    union() {
        cube([8,10,5], center=true);
        cube([6,10,11], center=true);
        cube([2,10,15], center=true);
        translate([4.5,0,0]) {
            cube([9,10,15], center=true);
            translate([0,0,-7.5]) {
                cube([9,10,15], center=true);
            }
        }

        translate([1.5,0,-55/2+15/2]) {
            cube([3, 10, 55], center=true);
        }
        translate([1.5,4,-55+15/2]) {
            rotate([90,0,90]) {
                cylinder(3, d=18, center=true);
            }
        }
        translate([3,4,-55+15/2]) {
            rotate([0,90,0]) {
                nupsies(8, 5);
            }
        }
    }
    union() {
        translate([0,4,-55+15/2]) {
            rotate([0,90,0]) {
                cylinder(50, d=5, center=true);
            }
        }
        translate([0,0,0]) {
            rotate([0,90,0]) {
                cylinder(50, d=5, center=true);
            }
        }
        translate([10.5,0,-15]) {
            rotate([90,0,0]) {
                cylinder(25, d=15, center=true);
            }
        }
        translate([4,-1,-18]) {
            sphere(d=4);
        }
        translate([4,1,-18]) {
            sphere(d=4);
        }
        translate([4,-1,-35]) {
            sphere(d=4);
        }
        translate([4,1,-35]) {
            sphere(d=4);
        }
        translate([4,0,-18]) {
            rotate([90,0,0]) {
                cylinder(h=2, d=4, center=true);
            }
        }
        translate([4,0,-35]) {
            rotate([90,0,0]) {
                cylinder(h=2, d=4, center=true);
            }
        }
        translate([4,1,-35]) {
            cylinder(35-18, d=4);
        }
        translate([4,-1,-35]) {
            cylinder(35-18, d=4);
        }
        translate([4,0,-(35-18)/2-18]) {
            cube([4, 2, 35-18], center=true);
        }
        translate([18,0,0]) {
            rotate([0,90,0]) {
                cylinder(d=8, h=22, center=true);
            }
        }
    }
}
