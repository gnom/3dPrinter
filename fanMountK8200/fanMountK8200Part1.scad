$fn=100;
difference() {
    cube([45 , 18, 20], center=true);
    union() {
        translate([0, -1, 10]) {
            cube([40, 11, 20], center=true);
        }
        translate([0,0,24]) {
            rotate([90 ,0 ,0]) {
                cylinder(25, d=38, center=true);
            }
        }
        translate([11, 0 ,0]) {
            cylinder(50, d=5, center=true);
        }
        for(i=[-1:2:1]) {
            translate([i*16, 0, 4]) {
                rotate([90,0,0]) {
                    cylinder(50, d=4, center=true);
                }
                translate([0,9,0]) {
                    rotate([90, 0 ,0]) {
                        cylinder(6.2, d=8, center=true, $fn=6);
                    }
                }
            }
        }
        cylinder(4, d=7, center=true);
        cylinder(45, d=4, center=true);
        $fn=30;
        for(i=[0:45:360]) {
            echo(i);
            rotate(i, 0, 0) {
                translate([5,0,-10]) {
                    sphere(d=2);
                }
            }
        }
    }
}
