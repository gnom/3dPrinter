$fn=150;

difference() {
    union() {
        cylinder(d=35, h=10, center=true);       
    }
    translate([0,0,-5]) {
        cylinder(d=8, h=8, center=true);
    }
     translate([0,0,4]) {
        scale([0.06,0.06, 0.1]) {
            rotate([0,180,0]) {
                mirror([180,0,0])
                surface("img/pesthoernchen.png", center=true, invert=true);
            }
        }
    }
}

for (i=[1:sqrt(pow(3,3)+pow(3,3)):360]) {
    rotate(i,0,0) {
        translate([17, 0, 0]) {
            rotate([0,0,45]) {
                cube([3,3,10], center=true);
            }
        }
    }
}
       
